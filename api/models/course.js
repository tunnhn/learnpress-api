let extend = require('extend');
let mysql = require('../../lib/mysql');
let request = require('../../lib/request');
let PHPUnserialize = require('php-unserialize');

let CourseX = function (req) {
    return new Course(req);
};
let Course = function (req) {
    this.request = request(req);
}

extend(Course.prototype, {
    getCourseMeta: async function (id) {
        var data = await mysql.querySync("SELECT * FROM {prefix}postmeta WHERE post_id = " + id);
        var meta = {};

        for (let i = 0, n = data.length; i < n; i++) {

            try {
                meta[data[i].meta_key] = PHPUnserialize.unserialize(data[i].meta_value);
            } catch (e) {
                meta[data[i].meta_key] = data[i].meta_value;
            }
        }

        return meta;
    },
    getList: function (options) {
        let self = this;
        let r = this.request;
        let sql = "SELECT p.*, pm.meta_value thumb_id " +
            "FROM {prefix}posts p " +
            "INNER JOIN {prefix}postmeta pm ON p.ID = pm.post_id AND pm.meta_key = '_thumbnail_id' " +
            "WHERE post_type = 'lp_course'";
        if(options && options.post__in){
            if(typeof options.post__in === 'string'){
                sql += " AND ID IN("+options.post__in+")"
            }else{
                sql += " AND ID IN("+options.post__in.join(',')+")"
            }
        }

        let s = r.get('keywords'),
            limit = r.get('limit'),
            page = r.get('page');

        if (s) {
            sql += " AND (post_title LIKE '%" + s + "%' OR post_content LIKE '%" + s + "%')";
        }

        if (limit > 0) {
            sql += " LIMIT " + ((page - 1) * limit) + ', ' + limit;
        }

        return new Promise(function (resolve, reject) {
            mysql.query(sql, async function (err, rows, fields) {
                if (err || !rows) {
                    return reject(err);
                }

                rows = JSON.parse(JSON.stringify(rows));
                let results = [];

                for (var i = 0; i < rows.length; i++) {
                    results.push(await self.parseCourse(rows[i]));
                }

                resolve(results);
            });
        })
    },
    getCourse: function (id) {
        return this.getList({post__in: id})
    },
    getCoursePrice: function (id) {
        return mysql.querySync("SELECT meta");
    },
    getThumbnail: async function (id) {
        let sql = "SELECT meta_key k, meta_value v FROM {prefix}postmeta WHERE post_id = " + id + " AND meta_key = '_wp_attachment_metadata'";
        let rows = mysql.query(sql);
        return rows;// ? rows[0].meta_value : '';
    },
    parseCourse: async function (data) {
        var images = await this.getThumbnail(data.thumb_id);
        var course = {};
        var meta = await this.getCourseMeta(data.ID);
        let siteURL = await mysql.getSiteUrl(),
            mapFields = {
                id: 'ID',
                post_title: 'name',
                post_name: 'slug',
                post_status: 'status',
                post_content: 'content'
            }, results = [];

        for (var k in mapFields) {
            if (!mapFields.hasOwnProperty(k)) {
                continue;
            }

            course[mapFields[k]] = data[k];
        }


        course.id = data.ID;
        course.thumbnail = {};
        course.permalink = siteURL;
        if (images) {
            try {
                let data = PHPUnserialize.unserialize(images[0].v);

                course.thumbnail.width = data.width;
                course.thumbnail.height = data.height;
                course.thumbnail.url = siteURL + '/wp-content/uploads/' + data.file;
                course.thumbnail.sizes = {};

                let p = data.file.split('/');
                p.pop();
                p = p.join('/');
                for (var j in data.sizes) {
                    if (!data.sizes.hasOwnProperty(j)) {
                        continue;
                    }

                    course.thumbnail.sizes[j] = {
                        width: data.sizes[j].width,
                        height: data.sizes[j].height,
                        url: siteURL + '/wp-content/uploads/' + p + '/' + data.sizes[j].file
                    };
                }


            } catch (e) {
            }
        }

        //course.meta = meta;
        course.price = meta['_lp_price'];
        course.duration = meta['_lp_duration'];
        course.maxStudents = meta['_lp_max_students'];
        course.retakeCount = meta['_lp_retake_count'];
        course.passingCondition = meta['_lp_passing_condition'];
        course.requiredEnroll = meta['_lp_required_enroll'];
        course.students = meta['_lp_students'];

        return course;
    }
});

exports = module.exports = CourseX;