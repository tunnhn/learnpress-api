module.exports = function (app) {
    let courseController = require('../controllers/course');
    let router = app.express.Router();

    // Check permission
    router.get('/api/courses', courseController.checkPermission);
    //router.post('/api/courses', courseController.checkPermission);

    // Success
    router.get('/api/courses', courseController.getList);
    //router.post('/api/courses', courseController.getList);

    router.get('/test/course/:id', courseController.checkPermission);
    router.get('/test/course/:id', courseController.getCourse);



    router.post('/api/course-create', courseController.checkPermission);
    router.post('/api/course-create', function (req, res, next) {
        res.send('asdasd')
    });


    app.use('/', router);

    // app.route('/api/courses')
    //     .get(courseController.checkPermission, courseController.getList);
    //
    // app.route('/api/courses/:id')
    //     .get(function(a, b, c){console.log('XXXX'); c();}, courseController.checkPermission);
};