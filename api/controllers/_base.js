const jwt = require('jsonwebtoken');

function LearnPress_API_Base() {
    return this;
}

module.exports = LearnPress_API_Base;

LearnPress_API_Base.LearnPress_API_Base = LearnPress_API_Base;

LearnPress_API_Base.prototype.checkPermission = function (req, res, next) {
    let token = req.method === 'POST' ? req.body.token : req.query.token;
    jwt.verify(token, 'lp-api', function (err, decode) {

        if (err) {
            res.send('Forbidden Access.');
            return;
        }
        req.tokenData = decode;
        next();
    });

};
