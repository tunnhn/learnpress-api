const util = require('util');
const extend = require('extend');
const mysql = require('../../lib/mysql');
const request = require('../../lib/request');
const CourseModel = require('../models/course');

function Course() {


    return this;
}

util.inherits(Course, require('./_base').LearnPress_API_Base);

extend(Course.prototype, {
    getList: function (req, res, next) {

        var course = new CourseModel(req);

        course.getList()
            .then(function (rows) {
                // for (var i = 0; i < rows.length; i++) {
                //     rows[i]['images'] = course.getThumbnail(rows[i].ID, function (r) {
                //
                //     });
                // }
                return rows;
                //// console.log(rows);
            })
            .then(function (rows) {
                res.send(rows);
            });
        // let r = request(req);
        // let sql = "SELECT * FROM wp_posts WHERE post_type = 'lp_course'";
        // let s = r.get('keywords'),
        //     limit = r.get('limit'),
        //     page = r.get('page');
        //
        // if (s) {
        //     sql += " AND (post_title LIKE '%" + s + "%' OR post_content LIKE '%" + s + "%')";
        // }
        //
        // if (limit > 0) {
        //     sql += " LIMIT " + ((page - 1) * limit) + ', ' + limit;
        // }
        //
        // mysql.query(sql, function (err, rows, fields) {
        //     if (err || !rows) {
        //         return res.send([]);
        //     }
        //
        //     rows = JSON.parse(JSON.stringify(rows));
        //     for (var i = 0; i < rows.length; i++) {
        //         rows[i].images = [123123];
        //     }
        //     res.send(rows);
        // });

    },
    getCourse: function (req, res, next) {
        var course = new CourseModel(req);
        var r = request(req);
        //res.send(r.get('id'));
        course.getCourse(r.get('id'))
            .then(function (rows) {
                res.send(rows);
            });
    },
    createCourse: function (req, res, next) {

        res.send('created');
    }
});

exports = module.exports = new Course();
