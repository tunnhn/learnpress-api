/**
 * Main file for app.
 *
 */

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const courseRoute = require('./api/routes/course');
const API_Manage = require('./lib/api-manage');
var router = express.Router();

var http = require('http');
const port = 1234;
const viewPath = __dirname + '/views/';
let apis = [];

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + '/public'));

router.use(function (a, b, next) {
    console.log('Load');
    next();
});

async function test() {

}

app.use('/', router);

app.get('/', function (req, res) {
    res.sendFile(viewPath + 'api-manage.html')
});

// test
app.get('/test', function (req, res) {

    // var request = http.get('http://localhost/learnpress/dev/', function (r) {
    //     var data = '';
    //     r.on('data', function (chunk) {
    //         data += chunk;
    //     });
    //     r.on('end', function () {
    //         //console.log(data);
    //         res.send(data);
    //     });
    //
    //
    // });
    // //console.log(request)
    res.sendFile(viewPath + '/test/index.html')
});
app.get('/test/courses', function (req, res) {
    res.sendFile(viewPath + '/test/get-courses.html')
});

//router.get('/test/course-create', courseController.checkPermission);
router.get('/test/course-create', function (req, res, next) {
    res.sendFile(viewPath + 'test/course.html')
});


/////

app.express = express;
courseRoute(app);

/**
 * Verify token
 *
 * @param token
 * @returns {Promise}
 */
function verifyToken(token) {
    return new Promise(function (resolve, reject) {
        jwt.verify(token, 'test', function (err, decode) {
            console.log('Verifiy');
            err ? reject() : resolve(decode);
        });
    })
}

API_Manage.on('connected', function (a) {
    API_Manage.get().then(function (r) {
        apis = r;
    });
}).connect();
initSocket();

function initSocket() {
    io.on('connection', socket => {
        API_Manage.get().then(function (r) {
            apis = r;
        });

        console.log('New user')
        ///socket.emit('socket ready x', true);

        socket.on('login', msg => {
            let {usr, pwd} = msg;
            let mysql = require('./lib/mysql');

            mysql.login(usr, pwd).then(function (user) {
                let u = {uid: user.ID, email: user.user_email, user_login: user.user_login},
                    authData = {
                        token: jwt.sign(u, 'test'),
                        user: u,
                        apis: apis
                    };

                socket.emit('logged in', authData);
            }, function (r) {
                socket.emit('login failed', 'Invalid username or password!');
            })
        });

        socket.on('verify-token', function (msg) {
            verifyToken(msg.token).then(function (user) {
                socket.emit('token-authorized', {user: user, token: msg.token, apis: apis});
            }, function (err) {
                socket.emit('token-authorized', err);
            });

            console.log(msg)
        });

        socket.on('save-api', function (msg) {


            verifyToken(msg.token).then(function (user) {

                API_Manage.save(msg.userId, msg.permission, msg.api).then(function (r) {

                    API_Manage.get().then(function (r) {
                        apis = r;
                    });

                    socket.emit('api-saved', r);

                });
                // console.log('create api:', msg);
                // let api = msg.api;
                // api.id = Date.now();
                // socket.emit('api-created', api);

            }, function (err) {
                socket.emit('token-authorized', err ? err : 'Forbidden Access.');
            })
        });

        socket.on('delete-api', function (msg) {

            verifyToken(msg.token).then(function (user) {

                API_Manage.delete(msg.userId, msg.permission, msg.api);

            }, function (err) {
                socket.emit('token-authorized', err ? err : 'Forbidden Access.');
            })
        });

        socket.on('disconnect', () => {
            console.log('User disconnected');
        });


    });
}


/**
 * Config routes.
 */
//app.use(require('./routes'))

/**
 * Listen.
 */
server.listen(port, () => {
    console.log('Listening on port ' + port + '...');
});