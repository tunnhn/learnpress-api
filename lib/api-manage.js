const mongodb = require('mongodb');
const extend = require('extend');
const MongoClient = mongodb.MongoClient;
const url = 'mongodb://admin:admin1234@ds241493.mlab.com:41493/nodejs';
const jwt = require('jsonwebtoken');

function API_Manage(url) {
    let self = this;

    this.db = null;
}

extend(API_Manage.prototype, {
    get: function () {
        let self = this;

        return new Promise(function (resolve, reject) {
            self.db && self.db.collection('learnpress_apis').find({}).toArray((err, result) => {
                if (err) return reject(err);

                resolve(result);
            })
        });
    },
    delete: function (userId, permission, api) {
        let self = this;
        //console.log(userId);
        return new Promise(function (resolve, reject) {
            let collection = self.db.collection('learnpress_apis');
            console.log(userId);
            collection.deleteOne();
        });
    },

    save: function (userId, permission, api) {
        let self = this;

        return new Promise(function (resolve, reject) {

            let collection = self.db.collection('learnpress_apis');
            collection.findOne(mongodb.ObjectId(api._id), function (e, r) {
                console.log({user_id: userId, permission: permission})
                let token = jwt.sign({user_id: userId, permission: permission}, 'lp-api');

                if (!api._id) {
                    collection.insert(api, function (err, result) {
                        if (err) {
                            reject(err);
                        }

                        collection.findOne(mongodb.ObjectId(api._id), function (e, r) {
                            r['token'] = token;
                            collection.save(r, mongodb.ObjectId(api._id))
                        });

                        resolve(result.ops[0]);
                    });
                } else {
                    if (r && r._id == api._id) {
                        r.name = api.name;
                        r.desc = api.desc;
                        r.token = token;

                        collection.save(r, mongodb.ObjectId(api._id), function (err, result) {
                            if (err) {

                                reject(err);
                            }

                            resolve(r);
                        });

                        console.log('Update:', r, api)
                    }
                }
            });
        });
    },

    connect: function () {
        let self = this;
        MongoClient.connect(url, function (err, client) {
            if (err) {
                console.log('Unable to connect to the mongoDB server. Error:', err);
            } else {
                self.db = client.db('nodejs');
                self.emit('connected', self);
            }
        });
    }
});

require('util').inherits(API_Manage, require('events').EventEmitter);

exports = module.exports = new API_Manage(url);