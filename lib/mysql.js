let mysql = require('mysql');
let extend = require('extend');
let jwt = require('jsonwebtoken');
let util = require('util');
let prefix = 'wp_';
let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'ob_learnpress_dev'
});

function MySql() {
    connection.connect();
    //connection.query = util.promisify(connection.query)
}

extend(MySql.prototype, {
    connection: function () {
        return connection;
    },
    query: async function (sql, cb) {
        sql = sql.replace(/{prefix}/g, prefix);

        if (cb) {
            connection.query(sql, cb);
        } else {
            return new Promise(function (a, b) {
                connection.query(sql, {}, function (err, rows) {
                    if (err) {
                        b(err);
                    } else {
                        a(rows);
                    }
                });
            });
        }
    },
    querySync: async function (sql) {
        sql = sql.replace(/{prefix}/g, prefix);

        let self = this;

        async function query() {
            return self.query(sql);
        }

        return await query();
    },
    getSiteUrl: async function () {
        let rows = await this.querySync("SELECT option_value FROM {prefix}options WHERE option_name = 'siteurl'");
        return rows ? rows[0].option_value : '';
    },
    getCourses: function () {

    },
    createApi: function (data) {
        return new Promise(function (resolve, reject) {

            connection.query("INSERT", function (err, rows, fields) {
                if (err) reject(err);

                let user = rows[0];
                let hasher = require('wordpress-hash-node');
                let checked = hasher.CheckPassword(pwd, user.user_pass);

                if (checked) {
                    resolve(user);
                } else {
                    reject(false);
                }
            });
        })

    },
    login: function (usr, pwd) {

        return new Promise(function (resolve, reject) {

            connection.query("SELECT * FROM wp_users where user_login = '" + usr + "'", function (err, rows, fields) {
                if (err) reject(err);

                let user = rows[0];
                let hasher = require('wordpress-hash-node');
                let checked = hasher.CheckPassword(pwd, user.user_pass);

                if (checked) {
                    resolve(user);
                } else {
                    reject(false);
                }
            });
        })

    }
});

exports = module.exports = new MySql();