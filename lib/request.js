function Request(request) {
    return new RequestX(request);
}

function RequestX(request) {
    this.request = request;
}

RequestX.prototype.get = function (name) {
    return this.request.method === 'POST' ? this.request.body[name] : (this.request.query[name] !== undefined ? this.request.query[name] : this.request.params[name]);
}

exports = module.exports = Request;
