// npm i nodemon mongodb mysql express extend gulp-sass jsonwebtoken middleware mongoose socket.io wordpress-hash-node -g
var gulp = require('gulp'),
    scss = require('gulp-sass');

gulp.task('scss', function () {
    return gulp.src(['public/scss/**/*.scss'])
        .pipe(scss())
        .pipe(gulp.dest('public/css'))
});


gulp.task('watch', function () {
    gulp.watch(['public/scss/**/*.scss'], ['scss']);
});